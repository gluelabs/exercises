console.info('DESTRUCTURING');

var object = { "a": 1, "b": 2 };

var { a, b } = object;

// a = 1
// b = 2

// Usato principalmente per l'importazione di moduli