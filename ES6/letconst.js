console.info('LET and CONST');

/*
 * Entrambe block-scoped.
 *  - let permette la riassegnazione
 *  - const solleva un errore
 *
 */

function f() {
  {
    const x = {};
    {
      //const x = "sneaky";
      // error, const
      x.a = "foo";
    }
    // error, already declared in block
    //x = "inner";
  console.log(x);
  }
}

f();