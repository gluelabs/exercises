export function root(x) {
  return x * x;
}

export function sum(x, y) {
  return x + y;
}

/**
 * Main.js:
 * 
 * import root from 'modules/math'; 
 * root(42);
 * 
 * ---------------
 * 
 * import { root } from 'modules/math'; 
 * math.root(42); math.sum(42, 24);
 * 
 * ---------------
 *  
 * 
 */