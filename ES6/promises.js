console.info('PROMISES');

function timeout(duration = 0) {
    console.log('Asking for a new Promise');
    return new Promise((resolve, reject) => {
        setTimeout(reject, duration);
    });
}

setTimeout(() => {
    console.log('Timeout');
}, 0);

let p = timeout(0)
    .then(
        () => { return timeout(2000); console.log('Success'); },
        () => { console.log('Errore'); }
    )
    .then(() => { console.log('Ended the promises chain'); });

console.log('Fine');