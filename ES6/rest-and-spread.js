console.info('REST ARGUMENTS');

function printArguments(...args) {
    args.forEach(function(arg) {
        console.log('rest args:', arg)
    });
}

printArguments(1, 2, 3, 4, 5);

console.info('.. AND SPREAD OPERATOR');

function sumEverything(x, y, z) {
    return x + y + z;
}

const nums = [1, 2, 3];
const letters = ['a', 'b', 'c'];
const result = sumEverything(...nums);

nums = [...nums, ...letters];
