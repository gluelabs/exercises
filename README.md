## Angular 2

A simple starter project demonstrating the basic concepts of Angular 2.

This project uses [Webpack](https://webpack.github.io/) for Development.

### Usage
- Clone or fork this repository
- Make sure you have [node.js](https://nodejs.org/) installed version 5+
- Make sure you have NPM installed version 3+

- run `npm install` to install dependencies
- run `npm run typings-install` to install typings
- run `npm start` to fire up dev server
- open browser to [`http://localhost:3000`](http://localhost:3000)