/** 
 * 
 * A decorator is a function that is going to decorate (populate) a target.
 * A target can be: class, property, method, parameter, accessor (getter or setter)
 * A decorator might look like:
 * 
 */ 
function myDecorator(target, key, descriptor) { } 

// For example: 
function clean(target) { 
    target.cleaned = true; 
} 

@clean 
class Animal {} // cleaned will be set to true 

//You can also stack decorators: 
@clean @dirty 
class Animal {} 

or 

@clean 
@dirty 
class Animal {} 

//Another example: 

function clean(value: boolean) { 
    return function (target) { 
        target.cleaned = value; 
    } 
} 

@clean(true)
class Animal { 
    open() {} 
} 