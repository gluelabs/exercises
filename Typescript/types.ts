
interface Vehicle {
    type: string;
    wheels: number;
}

class Mazda implements Vehicle {
    public type: string;

    constructor(type: string = 'a', public wheels: number = 1) {
        this.type = type;
    }
}

let car = new Mazda('5', 5);