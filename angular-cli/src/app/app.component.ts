import { Component, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'app-root',
  providers: [],
  styles: [`
    h1 {
      background-color: red;
      color: white;
    }

    li a.active {
      background-color: red;
      color: white;
      font-weight: bold;
    }
  `],
  template: `
    <nav class="navbar navbar-default">
      <div class="container-fluid">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href=""> {{title}} </a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
          <ul class="nav navbar-nav">
              <li class="nav-item">
                <a class="nav-link" routerLink="/directives" routerLinkActive="active"> Directives </a>
              </li>
              <li class="nav-item">
                <a class="nav-link" routerLink="/databinding" routerLinkActive="active"> Databinding </a>
              </li>
              <li class="nav-item">
                <a class="nav-link" routerLink="/jokes" routerLinkActive="active"> Jokes </a>
              </li>
              <li class="nav-item">
                <a class="nav-link" routerLink="/counter" routerLinkActive="active"> Counter </a>
              </li>
              <li class="nav-item">
                <a class="nav-link" routerLink="/pipes" routerLinkActive="active"> Pipes </a>
              </li>
              <li class="nav-item">
                <a class="nav-link" routerLink="/forms" routerLinkActive="active"> Forms </a>
              </li>
              <li class="nav-item">
                <a class="nav-link" routerLink="/canvas" routerLinkActive="active"> Canvas </a>
              </li>
            </ul>
        </div><!-- /.navbar-collapse -->
      </div><!-- /.container-fluid -->
    </nav>


    <div class="container">
      <router-outlet></router-outlet>
    </div>
  `
})
export class AppComponent {
  title = 'Angular works!';
}
