import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

// Declarations
import { AppComponent } from './app.component';

// Modules
import { AppRoutingModule } from './app.routes';
import { DatabindingModule } from './databinding';
import { CounterModule } from './counter';
import { DirectivesModule } from './directives';
import { JokesModule } from './jokes';
import { PipesModule } from './pipes';
import { MyFormsModule } from './forms';
import { CanvasModule } from './canvas';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    CounterModule,
    DatabindingModule,
    DirectivesModule,
    JokesModule,
    PipesModule,
    MyFormsModule,
    CanvasModule,
    AppRoutingModule
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
