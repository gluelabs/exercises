import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { PageNotFoundComponent }   from './page-not-found.component';

const routes = [
    { path: '404', component: PageNotFoundComponent },
    { path: '', redirectTo: 'directives', pathMatch: 'full' },
    {
        path: 'forms',
        loadChildren: 'app/forms/forms.module#MyFormsModule',
        data: {
            preload: true
        }
    },
    { path: '**', redirectTo: '404', pathMatch: 'full' }
];

@NgModule({
    imports: [
        RouterModule.forRoot(routes)
    ],
    declarations: [PageNotFoundComponent],
    exports: [RouterModule]
})
export class AppRoutingModule {}