import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
// Import RxJs required methods
import 'rxjs/add/observable/fromEvent';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/pairwise';

@Component({
    template: `
        <h3> Canvas </h3>
        <canvas height="500" width="500" id="portrait"></canvas>
    `,
    styles: [`canvas { border: 2px solid lightgrey }`]
})
export class CanvasComponent implements OnInit {
    lines: any[] = [];

    constructor() {}

    ngOnInit() {

        Observable
            .fromEvent<MouseEvent>(document.getElementById('portrait'), 'mousemove')
            .map((e: MouseEvent) => {
                return { x: event.clientX, y: event.clientY }
            })
            .pairwise()
            .map(
                positions => {
                    const p1 = positions[0];
                    const p2 = positions[1];
                    return { x1: p1.x, y1: p1.y, x2: p2.x, y2: p2.y }                    
                }
            )
            .subscribe(line => this.draw(line));
    }

    draw(line) {
        var c = <HTMLCanvasElement>document.getElementById('portrait');
        var ctx = c.getContext("2d");
        ctx.beginPath();
        ctx.moveTo(line.x1 - c.offsetLeft, line.y1 - c.offsetTop);
        ctx.lineTo(line.x2 - c.offsetLeft, line.y2 - c.offsetTop);
        ctx.stroke();
    }
    
}