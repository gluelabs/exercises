import { NgModule } from '@angular/core';

import { RouterModule }   from '@angular/router';
import { CanvasComponent }   from './canvas.component';

@NgModule({
    imports: [
        RouterModule.forChild([
            { path: 'canvas', component: CanvasComponent }
        ])
    ],
    exports: [],
    declarations: [CanvasComponent],
    providers: []
})
export class CanvasModule {}