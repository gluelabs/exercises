import { Component, OnInit } from '@angular/core';
import { CounterService } from './counter.service';

@Component({
    selector: 'counter-one',
    template: `
        <h3> Counter 1 </h3>
        <button (click)="componentIncrement()"> Increment counter </button> 
        <p> {{ counterCount }} </p>
    `,
    providers: [CounterService],
})
export class CounterComponent implements OnInit {
    ngOnInit() { }

    constructor(private counter: CounterService) {}

    componentIncrement() {
        this.counter.increment();    
    }

    get counterCount() {
        return this.counter.count;
    }

}