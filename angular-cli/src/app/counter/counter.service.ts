import { Injectable } from '@angular/core';

@Injectable()
export class CounterService {
    private _count: number = 0;

    constructor() { }

    increment() {
        this._count++;
        console.log(`Count is now ${this._count}`);
    }

    get count() {
        return this._count;
    }
}
