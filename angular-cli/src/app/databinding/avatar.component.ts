import {
    Component,
    OnInit,
    Input,
    Output,
    EventEmitter
} from '@angular/core';

@Component({
    selector: 'avatar',
    template: `
        <div>
            <span (click)="onClick('name')"> {{ user?.name }} </span>
            <span (click)="onClick('surname')"> {{ user?.surname }} </span>
        </div>
    `
})
export class AvatarComponent implements OnInit {
    @Input() user: any;
    @Output('onAvatarNameClick') onNameClick: EventEmitter<any> = new EventEmitter();
    
    ngOnInit() { }

    onClick(target) {
        this.onNameClick.emit(target);
    }
}