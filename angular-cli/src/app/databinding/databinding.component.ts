import { Component, ViewEncapsulation } from '@angular/core';

@Component({
    selector: 'databinding-cmp',
    template: `
        <h3> This is the databinding component </h3>
        <p> {{ sayHi() }} </p>
        <img [src]="activeImage" />
        <button class="btn btn-default" (click)="onBtnClick()"> Click me </button>
        <hr>
        <input class="form-control" [(ngModel)]="myName">
        <p> {{ myName }} </p>
        <hr>
        <input class="form-control" #mysurname (keyup)="0">
        <p> {{ mysurname.value }} </p>
        <br>
        <avatar [user]="myUser" (onAvatarNameClick)="onMyNameClick($event)"></avatar>
    `
})
export class DatabindingComponent {
    myName: string;
    urls: Array<string> = [
        'http://lorempixel.com/100/100/sport',
        'http://lorempixel.com/100/100/nature',
        'http://lorempixel.com/100/100/food'
    ];
    activeImage: string = this.urls[0];
    
    myUser = {
        name: 'Massimiliano',
        surname: 'Sartoretto'
    };

    onBtnClick() {
        this.activeImage = this.urls[Math.floor(Math.random() * 3)];
    }

    sayHi() {
        return 'Hi';
    }

    onMyNameClick(evt) {
        console.log(evt);
    }
}