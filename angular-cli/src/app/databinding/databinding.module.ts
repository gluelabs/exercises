import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';

import { DatabindingComponent }   from './databinding.component';
import { AvatarComponent }   from './avatar.component';

@NgModule({
    imports: [
        BrowserModule,
        FormsModule,
        RouterModule.forChild([{
            path: 'databinding', component: DatabindingComponent
        }])
    ],
    declarations: [
        AvatarComponent,
        DatabindingComponent
    ],
    providers: []
})
export class DatabindingModule { }
