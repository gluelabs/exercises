import { Component } from '@angular/core';

@Component({
    selector: 'attribute-directives',
    template: `
        <h3> Attribute directives </h3>
        <select [(ngModel)]="selectedState" class="form-control">
            <option *ngFor="let s of states" [value]="s.class"> {{ s.label }} </option>
        </select>

        <input class="form-control" type="text" [(ngModel)]="divHeight">
        <div class="alert" [collapse]="isCollapsed" (click)="toggleCollapse()" 
            [ngClass]="selectedState" [ngStyle]="{'height': divHeight + 'px'}">
            <p> Some content </p>
        </div>
    `
})
export class AttributeDirectivesComponent {

    isCollapsed: boolean = false;
    states: Array<any> = [{
        label: 'success',
        class: 'alert-success' 
    }, {
        label: 'warning',
        class: 'alert-warning' 
    }, {
        label: 'danger',
        class: 'alert-danger' 
    }];

    toggleCollapse() {
        this.isCollapsed = !this.isCollapsed;
    }

}