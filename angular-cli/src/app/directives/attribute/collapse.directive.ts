import {
    Directive,
    Input,
    ElementRef,
    OnChanges
} from '@angular/core';

@Directive({
    selector: '[collapse]'
})
export class CollpaseDirective implements OnChanges {
    @Input('collapse') isCollapsed: boolean;

    constructor(private el: ElementRef) {
        this.el.nativeElement.style.transition = 'all 1s';
    }

    ngOnChanges(changes) {
        console.log('-----------');        
        console.log(changes);  
        console.log('-----------');      
        if (this.isCollapsed) {
            this.el.nativeElement.style.height = '0px';
            this.el.nativeElement.style.padding = '0px';
            this.el.nativeElement.style.overflow = 'hidden';
        } else {
            this.el.nativeElement.style.height = '100px';
            this.el.nativeElement.style.padding = '15px';
        }
    }
}