import { Component, OnInit } from '@angular/core';

@Component({
    template: `
        <h2> Directives section </h2>
        <div class="col-md-6">
            <structural-directives></structural-directives>
        </div>
        <div class="col-md-6">
            <attribute-directives></attribute-directives>
        </div>
    `
})
export class DirectivesComponent implements OnInit {
    constructor() { }

    ngOnInit() { }
}