import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { RouterModule, Route } from '@angular/router';

import { DirectivesComponent }   from './directives.component';
import { StructuralDirectivesComponent } from './structural/structural.component';
import { AttributeDirectivesComponent } from './attribute/attribute.component';
import { CollpaseDirective } from './attribute/collapse.directive';
import { UnlessDirective } from './structural/unless.directive';

const routes: Array<Route> = [
    { path: 'directives', component: DirectivesComponent }
    //{ path: '**', redirectTo: '404', pathMatch: 'full' }
];

@NgModule({
    imports: [
        BrowserModule,
        FormsModule,
        RouterModule.forChild(routes)
    ],
    exports: [

    ],
    declarations: [
        DirectivesComponent,
        StructuralDirectivesComponent,
        AttributeDirectivesComponent,
        CollpaseDirective,
        UnlessDirective
    ],
    providers: [],
})
export class DirectivesModule { }
