import { Component } from '@angular/core';

@Component({
    selector: 'structural-directives',
    template: `
        <h3> Structural directives </h3>
        show name: <input type="checkbox" [(ngModel)]="isNameVisible" />
        
        <p *ngIf="isNameVisible"> Massimiliano </p>
        
        <template [ngIf]="isNameVisible">
            <p> Massimiliano </p>
        </template>

        <ul class="list-group">
            <li class="list-group-item"
                *ngFor="let f of fruits; let i = index">
                ({{ i }}) - {{ f.label }} {{ f.qta }}
            </li>
        </ul>

        <ul class="list-group">
            <template ngFor let-f [ngForOf]="fruits">
                ({{ i }}) - {{ f.label }} {{ f.qta }}
            </template>
        </ul>

        <hr>
        <p *unless="switchNumber == '50'"> Massimiliano </p>
        <input type="text" class="form-control" [(ngModel)]="switchNumber" />
        <div [ngSwitch]="switchNumber">
            <h4 *ngSwitchCase="'1'"> 1 </h4>
            <h4 *ngSwitchCase="'10'"> 10 </h4>
            <h4 *ngSwitchCase="'100'"> 100 </h4>
            <h4 *ngSwitchDefault> default </h4>
        </div>
    `
})
export class StructuralDirectivesComponent {
    fruits: Array<Object> = [{
        label: 'Bananas',
        qta: 1
    }, {
        label: 'Kiwis',
        qta: 5
    }, {
        label: 'Dragon fruit',
        qta: 3
    }];
}