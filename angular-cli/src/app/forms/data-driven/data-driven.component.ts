import { Component, OnInit, OnDestroy } from '@angular/core';
import {
    FormGroup,
    FormBuilder,
    Validators,
    FormControl
} from '@angular/forms';


@Component({
    styles: [`
        .ng-dirty.ng-invalid {
            border-color: red;
        }
        .ng-valid {
            border-color: green;
        }
    `],
    template: `
        <h3> Data driven form </h3>

        <form (ngSubmit)="onFormSubmit()" [formGroup]="myForm">
            <div formGroupName="userData">
                <div class="form-group">
                    <label> Email </label>
                    <input type="text" class="form-control"
                            id="email" name="email" formControlName="email">
                </div>
                <div class="form-group">
                    <label> Username </label>
                    <input type="text" class="form-control"
                            id="username" name="username" formControlName="username">
                </div>
            </div>
            <div class="form-group">
                <label> Password </label>
                <input type="password" class="form-control"
                        id="password" name="password" formControlName="password">
            </div>
            <button type="submit" class="btn btn-primary"
                    [disabled]="!myForm.valid">
                Save
            </button>

            <button (click)="clearForm()" class="btn btn-danger"
                    [disabled]="!myForm.dirty">
                Clear
            </button>
        
        </form>

    `
})
export class DataFormComponent implements OnInit, OnDestroy {
    
    myForm: FormGroup;
    subscription: any;

    constructor(private formBuilder: FormBuilder) {}

    ngOnInit() {
        this.myForm = this.formBuilder.group({
            'userData': this.formBuilder.group({
                'username': ['', Validators.required, this.timeoutValidator],
                'email': ['', Validators.required]
            }),
            'password': ['', [
                Validators.required,
                this.hasNumber
            ]]
        });

        this.subscription = this.myForm.valueChanges.subscribe(
            (data) => console.log(data)            
        );

        this.myForm.statusChanges.subscribe(
            (data) => console.log(`STATUS CHANGED - ${data}`)
        );

    }

    hasNumber(control: FormControl): {[s: string]: boolean} {
        if(regex.test(control.value)) {
            return null;
        }
        else {
            return { 'no-numbers': true };
        }    
    }

    timeoutValidator(control: FormControl): Promise<{[s: string]: boolean}> {

        return new Promise<any>(
            (resolve, reject) => {
                setTimeout(
                    () => {
                        if(control.value === 'pippo') {
                            resolve(null);
                        }
                        else {
                            resolve({'no-pippo': true});
                        }
                    }, 2000
                )
            }
        )
    }

    onFormSubmit() {
        console.log(this.myForm);
    }

    clearForm() {
        this.myForm.reset();
    }

    ngOnDestroy() {
        console.log('ngOnDestroy');
        this.subscription.unsubscribe();
    }
}