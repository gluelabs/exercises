import { Component, OnInit } from '@angular/core';

@Component({
    template: `
        <h2> Forms component </h2>
        <a routerLink="./template-driven"> Template driven </a>
        <a routerLink="./data-driven"> Data driven </a>

        <a class="pull-right" routerLink="/"> Back </a>

        <router-outlet></router-outlet>
    `
})
export class FormsComponent implements OnInit {
    constructor() { }

    ngOnInit() { }
}