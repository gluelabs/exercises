import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';

import { FormsComponent }   from './forms.component';
import { DataFormComponent }   from './data-driven/data-driven.component';
import { TemplateFormComponent }   from './template-driven/template-driven.component';

import { CanActivateGuard } from './template-driven-enter.guard';
import { CanDeactivateGuard } from './template-driven-exit.guard';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        RouterModule.forChild([
            {
                path: '',
                component: FormsComponent,
                children: [
                    {
                        path: 'data-driven',
                        component: DataFormComponent,
                    },
                    {
                        path: 'template-driven',
                        component: TemplateFormComponent,
                        canActivate: [CanActivateGuard],
                        canDeactivate: [CanDeactivateGuard]
                    },
                    {
                        path: '',
                        redirectTo: 'data-driven'
                    }
                ]
            }
        ])
    ],
    declarations: [
        FormsComponent,
        DataFormComponent,
        TemplateFormComponent
    ],
    exports: [],
    providers: [
        CanActivateGuard,
        CanDeactivateGuard
    ]
})
export class MyFormsModule { }