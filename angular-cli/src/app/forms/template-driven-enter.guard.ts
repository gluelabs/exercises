import { Injectable } from '@angular/core';
import { CanActivate } from '@angular/router';
import { Observable } from 'rxjs';

@Injectable()
export class CanActivateGuard implements CanActivate {
    constructor() {}
    
    canActivate() {
        return Observable.create(
            (observer) => {
                setTimeout(
                    () => {
                        observer.next(true),
                        observer.complete()
                    }, 1500
                )
            }
        )
    }
}