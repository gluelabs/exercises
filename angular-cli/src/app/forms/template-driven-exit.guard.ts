import { Injectable } from '@angular/core';
import { CanDeactivate } from '@angular/router';

import { TemplateFormComponent } from './template-driven/template-driven.component';

export class CanDeactivateGuard implements CanDeactivate<TemplateFormComponent> {
    canDeactivate(target: TemplateFormComponent) {
        if(target.hasChanges()) {
            return window.confirm('Are you sure you wanna leave?');
        }
        
        return true;
    }
}