import { Component, OnInit } from '@angular/core';

@Component({
    styles: [`
        .ng-dirty.ng-invalid {
            border-color: red;
        }
        .ng-valid {
            border-color: green;
        }
    `],
    template: `
        <h3> Template driven form </h3>

        <form (ngSubmit)="onFormSubmit(f)" #f="ngForm" novalidate>
            <div class="form-group">
                <label> Email </label>
                <input type="email" class="form-control" id="email"
                        name="email" [(ngModel)]="myemail"
                        #email="ngModel"
                        required>
                <span class="help-block text-error"
                    *ngIf="!email.valid || email.pristine">
                    Invalid email </span>
            </div>
            <div class="form-group">
                <label> Username </label>
                <input type="text" class="form-control" minlenght="8" id="username"
                        name="username" [(ngModel)]="user.username" required>
            </div>
            <div class="form-group">
                <label> Password </label>
                <input type="password" class="form-control" minlenght="8" id="password"
                        name="password" [(ngModel)]="user.password" required>
            </div>
            <button type="submit" class="btn btn-primary"
                [disabled]="!f.valid">
                Save
            </button>
        </form>

    `
})
export class TemplateFormComponent implements OnInit {
    
    user: Object = {};

    constructor() { }

    ngOnInit() { }

    onFormSubmit(form) {
        console.log(form);
    }

    hasChanges() {
        return true;
    }
}