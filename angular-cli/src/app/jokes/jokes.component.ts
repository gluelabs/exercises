import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Router } from '@angular/router';

import { JokesService } from './jokes.service';
import 'rxjs/add/operator/pairwise';

@Component({
    styles: [`
        .loading-spinner {
            text-align: center;
        }
        .loading-spinner > img {
            height: 50px;
        }
    `],
    template: `
        <h3> This is the Jokes component </h3>
        <button id="target-obs" class="btn btn-default"
                (click)="onButtonClick()"> Send request </button>
        
        <div class="loading-spinner col-md-12" *ngIf="isLoading">
            <img src="./assets/loading.gif"/>
        </div>
        <table class="table table-hover table-bordered">
            <tr>
                <th> Id </th>
                <th> Joke </th>
            </tr>
            <tr *ngFor="let j of jokeList" (click)="navigateJoke(j.id)">
                <td> {{ j.id }} </td>
                <td> {{ j.joke }} </td>
            </tr>
        </table>
    `,
    providers: [JokesService]
})
export class JokesComponent implements OnInit {
    jokeList: any;    
    isLoading: boolean = false;

    constructor(
        private jokes: JokesService,
        private router: Router
    ) { }

    ngOnInit() { 
        Observable
            .fromEvent(document.getElementById('target-obs'), 'click')
            .skip(3)
            .pairwise()
            .subscribe(
                () => console.log('Button clicked')
            );

        this.onButtonClick();
    }

    onButtonClick() {
        this.isLoading = true;
        this.jokes.getAll()
            .subscribe(
                (jokeList) => this.jokeList = jokeList,
                (err) => console.log('Error', err),
                () => this.isLoading = false               
            );
    }

    navigateJoke(id) {
        console.log(`Navigating ${id}`);
        this.router.navigate(['jokes', id]);
    }
}