import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { JokesComponent }   from './jokes.component';
import { SingleJokeComponent }   from './single-joke.component';

@NgModule({
    imports: [
        BrowserModule,
        RouterModule.forChild([
            { path: 'jokes', component: JokesComponent },
            { path: 'jokes/:id', component: SingleJokeComponent }
        ])
    ],
    declarations: [
        JokesComponent,
        SingleJokeComponent
    ],
    exports: [],
    providers: []
})
export class JokesModule { }