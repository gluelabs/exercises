import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Observable } from 'rxjs';
import 'rxjs/add/operator/map';


@Injectable()
export class JokesService {

    constructor(private http: Http) { }

    getRandom() : Observable<any> {
        return this.http
            .get('https://api.icndb.com/jokes/random')
            .map( (r) => {return r.json()} )
            .map( (r) => {return r.value} );
    }

    getAll() : Observable<any> {
        return this.http
            .get('https://api.icndb.com/jokes/')
            .map( (r) => {return r.json()} )
            .map( (r) => {return r.value} );
    }

    getById(id) : Observable<any> {
        return this.http
            .get(`https://api.icndb.com/jokes/${id}`)
            .map( (r) => {return r.json()} )
            .map( (r) => {return r.value.joke} );
    }
}