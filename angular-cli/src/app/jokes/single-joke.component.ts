import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { JokesService } from './jokes.service';

@Component({
    template: `
        <h3> This is the joke id {{ jokeId }}  </h3>
        <a routerLink="/jokes"> Back to list </a>
        <br>
        <span> {{ joke | async }} </span>
    `,
    providers: [JokesService]
})
export class SingleJokeComponent implements OnInit {
    joke: any;    
    jokeId: number;

    constructor(
        private jokes: JokesService,
        private route: ActivatedRoute
    ) { }

    ngOnInit() {
        
        this.route.params.forEach(
            (params) => {
                this.jokeId = +params['id'];
                this.joke = this.jokes.getById(this.jokeId);
            }
        );

    }
}