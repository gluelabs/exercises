import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name: 'filter',
    pure: false
})

export class FilterPipe implements PipeTransform {
    transform(array: string[], pattern: string): any {
        
        if (array && pattern && pattern.length) {
            return array.filter(
                (value) => value.toLowerCase().match(pattern.toLowerCase())
            );
        } else {
            return array;
        }

    }
}