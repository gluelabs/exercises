import { Component, OnInit } from '@angular/core';

@Component({
    template: `
        <h2> Pipes component </h2>
        <p> {{ today | date:'fullDate' }} </p>
        <!--
        <input type="text" class="form-control" [(ngModel)]="myVar" />

        <p> Up + Low: {{ myVar | uppercase | lowercase }} </p>

        <p> Capitalize: {{ myVar | capitalize }} </p>

        <p> Reverse: {{ myVar | reverse }} </p>

        <p> Cap + Rev: {{ myVar | capitalize | reverse | capitalize }} </p>
        -->
        <hr>
        <input type="text" class="form-control"
                [(ngModel)]="searchValue" placeholder="search"/>

        <input type="text" class="form-control"
                placeholder="insert" #addValue
                (keyup.enter)="addFruit(addValue.value)">

        <ul class="list-group">
            <li class="list-group-item"
                *ngFor="let f of fruits | filter:searchValue"> {{ f }} </li>
        </ul>
    `
})
export class PipesComponent implements OnInit {
    today = new Date();
    fruits: Array<string> = ['Bananas','Kiwis','Strawberries','Oranges'];
    constructor() { }

    ngOnInit() { }

    addFruit(f) {
        if(f) {
            this.fruits.unshift(f);
        }
    }
}