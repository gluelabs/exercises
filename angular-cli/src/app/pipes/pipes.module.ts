import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { PipesComponent }   from './pipes.component';
import { CapitalizePipe }   from './capitalize.pipe';
import { ReversePipe }   from './reverse.pipe';
import { FilterPipe }   from './filter.pipe';

@NgModule({
    imports: [
        BrowserModule,
        FormsModule,
        RouterModule.forChild([
            { path: 'pipes', component: PipesComponent }
        ])
    ],
    exports: [],
    declarations: [
        PipesComponent,
        CapitalizePipe,
        ReversePipe,
        FilterPipe
    ],
    providers: []
})
export class PipesModule {}