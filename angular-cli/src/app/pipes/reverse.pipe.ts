import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name: 'reverse'
})

export class ReversePipe implements PipeTransform {
    transform(input: string, args: any[]): any {
        
        if(input && input.length) {
            return input.split('').reverse().join('');
        } else {
            return input;
        }

    }
}